import { Injectable } from '@nestjs/common';
import { CreateProductDto } from './dto/create-product.dto';
import { UpdateProductDto } from './dto/update-product.dto';
import { Product } from './entities/product.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';

@Injectable()
export class ProductsService {
  constructor(
    @InjectRepository(Product) private productRepository: Repository<Product>,
  ) {}
  create(createProductDto: CreateProductDto) {
    return this.productRepository.save(createProductDto);
  }

  findAll() {
    return this.productRepository.find({ relations: { type: true } });
  }

  findOne(id: number) {
    return this.productRepository.findOne({
      where: { id },
      relations: { type: true },
    });
  }

  async update(id: number, updateProductDto: UpdateProductDto) {
    const updatedProduct = await this.productRepository.findOneOrFail({
      where: { id },
    });
    await this.productRepository.update(id, {
      ...updatedProduct,
      ...updateProductDto,
    });
    const result = await this.productRepository.findOne({
      where: { id },
      relations: { type: true },
    });
    return result;
  }

  async remove(id: number) {
    const deleteProduct = await this.productRepository.findOneOrFail({
      where: { id },
    });
    await this.productRepository.remove(deleteProduct);

    return deleteProduct;
  }
}
